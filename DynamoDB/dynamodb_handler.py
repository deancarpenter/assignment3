from __future__ import print_function
import boto3
import json
import sys
import decimal
from botocore.exceptions import ClientError
from boto3.dynamodb.conditions import Key, Attr
import re

# Note - Do not change the class name and constructor
# You are free to add any functions to this class without changing the specifications mentioned below.
class DynamoDBHandler:

    def __init__(self, region):
        self.client = boto3.client('dynamodb')
        #self.resource = boto3.resource('dynamodb', region_name=region)
        #self.dynamodb = boto3.resource('dynamodb', region_name='us-east-1', endpoint_url="http://localhost:8000")
        self.dynamodb = boto3.resource('dynamodb',region_name='us-east-1') 

    def help(self):
        print("Supported Commands:")
        print("1. insert_movie")
        print("2. delete_movie")
        print("3. update_movie")
        print("4. search_movie_actor")
        print("5. search_movie_actor_director")
        print("6. print_stats")
        print("7. delete_table")

    def create_and_load_data(self, tableName, fileName):
        # TODO - This function should create a table named <tableName> 
        # and load data from the file named <fileName>

        try:
            self.table = self.dynamodb.create_table(
                TableName='Movies',
                KeySchema=[
                    {
                        'AttributeName': 'year',
                        'KeyType': 'HASH'  # Partition key
                    },
                    {
                        'AttributeName': 'title',
                        'KeyType': 'RANGE'  # Sort key
                    }
                ],
                AttributeDefinitions=[
                    {
                        'AttributeName': 'year',
                        'AttributeType': 'N'
                    },
                    {
                        'AttributeName': 'title',
                        'AttributeType': 'S'
                    },

                ],
                ProvisionedThroughput={
                    'ReadCapacityUnits': 10,
                    'WriteCapacityUnits': 10
                },
            )

            self.populateMovieTable()

        # Table already exists, just instantiate our table variable
        except self.client.exceptions.ResourceInUseException:
            self.table = self.dynamodb.Table('Movies')
            print("Table already exists")

        print("Table status:", self.table.table_status)

    def populateMovieTable(self):
        # Poll until the table has been created
        self.table.wait_until_exists()

        with open("moviedata.json") as json_file:
            movies = json.load(json_file, parse_float = decimal.Decimal)
            for movie in movies:
                year = int(movie['year'])
                title = movie['title']
                info = movie['info']
                if('actors' in info):
                    info['query_actors'] = [actor.lower() for actor in info['actors']]

                if('directors' in info):
                    info['query_directors'] = [director.lower() for director in info['directors']]

                print("Adding movie:", year, title)

                self.table.put_item(
                   Item={
                       'year': year,
                       'title': title,
                       'query_title': title.lower(),
                       'info': info,
                    }
                )

    def insert_movie(self, year, title, directors, actors, release_date, rating):
        try:
            response = self.table.put_item(
               Item={
                    'year': int(year),
                    'title': title,
                    'query_title': title.lower(),
                    'info': {
                        'directors': directors,
                        'actors': actors,
                        'query_actors': [actor.lower() for actor in actors],
                        'query_directors': [director.lower() for director in directors],
                        'release_date': release_date,
                        'rating': decimal.Decimal(rating)
                    }
                }
            )

        except Exception as e:
            s = ""
            s += "Movie " + title + " could not be inserted - "
            s += "\n" + str(e)
            raise(e)

        else:
            return "Movie " + title + " successfully inserted"
            #print(json.dumps(response, indent=4, cls=DecimalEncoder))

    def get_movie(self, title):
        response = self.table.scan()

        movies = filter(lambda x: x['query_title'] == title.lower(), response['Items'])
        for movie in movies:
            print(movie['year'], ":", movie['title'])

        # Since scan only returns up to 1mb each time...
        while 'LastEvaluatedKey' in response:
            response = self.table.scan(
                ExclusiveStartKey=response['LastEvaluatedKey']
                )

            movies_to_delete = filter(lambda x: x['query_title'] == title.lower(), response['Items'])
            for movie in movies_to_delete:
                print(movie['year'], ":", movie['title'] + "\n" + str(movie['info']))          

    def delete_movie_by_title(self, title):
        # Tracks how many movies were deleted
        delete_counter = 0
        try:
            response = self.table.scan()

            movies_to_delete = filter(lambda x: x['query_title'] == title.lower(), response['Items'])
            for movie in movies_to_delete:
                deletion_response = self.table.delete_item(
                        Key={
                            'year': movie['year'],
                            'title': movie['title']
                        }
                    )
                delete_counter += 1
                

            # Since scan only returns up to 1mb each time...
            while 'LastEvaluatedKey' in response:
                response = self.table.scan(
                    ExclusiveStartKey=response['LastEvaluatedKey']
                    )

                movies_to_delete = filter(lambda x: x['query_title'] == title.lower(), response['Items'])
                for movie in movies_to_delete:
                    deletion_response = self.table.delete_item(
                            Key={
                                'year': movie['year'],
                                'title': movie['title']
                            }
                        )
                    delete_counter += 1

        except ClientError as e:
            s += "Movie " + title + " could not be deleted - "
            s += "\n" + e
            return s
            raise e
        else:
            if delete_counter > 0:
                return "Movie " + title + " successfully deleted"
            else:
                return "Movie: " + title + " does not exist"
            #print(json.dumps(response, indent=4, cls=DecimalEncoder))


    def update_movie(self, year, title, directors, actors, release_date, rating):
        try:
            response = self.table.update_item(
                Key={
                    'year': int(year),
                    'title': title
                },
                UpdateExpression="set info.directors=:d, info.actors=:a, info.query_actors=:qa," + 
                                "info.query_directors=:qd, info.release_date=:rd, info.rating=:r",
                ExpressionAttributeValues={
                        ':d': directors,
                        ':a': actors,
                        ':qa': [actor.lower() for actor in actors],
                        ':qd': [director.lower() for director in directors],
                        ':rd': release_date,
                        ':r': decimal.Decimal(rating),
                    },
            )
        except Exception as e:
            if e.response['Error']['Code'] == 'ValidationException':
                return "Movie with title " + title + " and year " + year + " does not exist"

        else:
            return "Movie " + title + " successfully updated"
            #print(json.dumps(response, indent=4, cls=DecimalEncoder))

    def search_movie_actor(self, actor):
        response_string = ""
        try:
            fe = Attr('info.query_actors').contains(actor.lower())
            response = self.table.scan(
                FilterExpression=fe
                )
            response_string += self.format_output(response['Items'])

            # Since scan only returns up to 1mb each time...
            while 'LastEvaluatedKey' in response:
                response = self.table.scan(
                    ExclusiveStartKey=response['LastEvaluatedKey'],
                    FilterExpression=fe
                    )
                response_string += self.format_output(response['Items'])
        except Exception as e:
            raise(e)

        else:
            #print(json.dumps(response, indent=4, cls=DecimalEncoder))
            if len(response_string) > 0:
                return response_string
            else:
                return "No movies found for actor: " + actor

    def search_movie_actor_director(self, actor, director):
        response_string = ""
        try:
            fe = Attr('info.query_actors').contains(actor.lower()) & Attr('info.query_directors').contains(director.lower())
            response = self.table.scan(
                FilterExpression=fe
                )
            response_string += self.format_output(response['Items'])

            # Since scan only returns up to 1mb each time...
            while 'LastEvaluatedKey' in response:
                response = self.table.scan(
                    ExclusiveStartKey=response['LastEvaluatedKey'],
                    FilterExpression=fe
                    )
                response_string += self.format_output(response['Items'])
        except Exception as e:
            raise(e)

        else:
            #print(json.dumps(response, indent=4, cls=DecimalEncoder))
            if len(response_string) > 0:
                return response_string
            else:
                return "No movies found for actor: " + actor + " and director: " + director

    def print_stats(self, stats):
        UNREASONABLY_LARGE_RATING = 100000000
        IMPOSSIBLY_LOW_RATING = -100000000

        response_string = ""

        # initialize rating depending on whether user wants lowest or highest rating
        rating = 0
        if stats == "highest_rating_movie":
            rating = IMPOSSIBLY_LOW_RATING
        else:
            rating = UNREASONABLY_LARGE_RATING
        list_of_same_ratings = []

        try:
            response = self.table.scan()
            for movie in response['Items']:
                if 'rating' in movie['info']:
                    if stats == "highest_rating_movie":
                        if movie['info']['rating'] > rating:
                            rating = movie['info']['rating']
                            del list_of_same_ratings[:]
                            list_of_same_ratings.append(self.format_print_stats_output(movie))

                        elif movie['info']['rating'] == rating:
                            list_of_same_ratings.append(self.format_print_stats_output(movie))

                    else:
                        if movie['info']['rating'] < rating:
                            rating = movie['info']['rating']
                            del list_of_same_ratings[:]
                            list_of_same_ratings.append(self.format_print_stats_output(movie))

                        elif movie['info']['rating'] == rating:
                            list_of_same_ratings.append(self.format_print_stats_output(movie))

            # Since scan only returns up to 1mb each time...
            while 'LastEvaluatedKey' in response:
                response = self.table.scan(
                    ExclusiveStartKey=response['LastEvaluatedKey'],
                    )

                for movie in response['Items']:
                    if 'rating' in movie['info']:
                        if stats == "highest_rating_movie":
                            if movie['info']['rating'] > rating:
                                rating = movie['info']['rating']
                                del list_of_same_ratings[:]
                                list_of_same_ratings.append(self.format_print_stats_output(movie))

                            elif movie['info']['rating'] == rating:
                                list_of_same_ratings.append(self.format_print_stats_output(movie))

                        else:
                            if movie['info']['rating'] < rating:
                                rating = movie['info']['rating']
                                del list_of_same_ratings[:]
                                list_of_same_ratings.append(self.format_print_stats_output(movie))

                            elif movie['info']['rating'] == rating:
                                list_of_same_ratings.append(self.format_print_stats_output(movie))

        except Exception as e:
            raise e

        else:
            # turn this array into strings on separate lines
            response_output = ""
            for rating in list_of_same_ratings:
                if rating == list_of_same_ratings[-1]:
                    response_output += (rating)
                else:
                    response_output += (rating + "\n")
            return response_output

    def delete_table(self, tableName):
        try:
            requested_table = self.dynamodb.Table(tableName)
            requested_table.delete()

        except Exception as e:
            if e.response['Error']['Code'] == "ResourceNotFoundException":
                return "Table " + tableName + " does not exist."
            print(e)
            raise(e)

        else:
            return "Table " + tableName + " successfully deleted"

    def format_output(self, movies):
        response_string = ""
        for movie in movies:
            keys = ['title', 'year', 'actors', 'directors', 'info']

            # filter top level JSON keys
            for key in movie.keys():
                if key not in keys or 'query' in key:
                    movie.pop(key, None)
            
            # filter the nested 'info' JSON object keys
            info = movie['info']
            for key in info.keys():
                if key not in keys or 'query' in key:
                    info.pop(key, None)

            if movie == movies[-1]:
                response_string += str(movie)
            else:
                response_string += str(movie) + "\n"
        return response_string

    def format_print_stats_output(self, movie):
        response_string = ""
        keys = ['title', 'year', 'actors', 'directors', 'info', 'rating']

        # filter top level JSON keys
        for key in movie.keys():
            if key not in keys or 'query' in key:
                movie.pop(key, None)
        
        # filter the nested 'info' JSON object keys
        info = movie['info']
        for key in info.keys():
            if key not in keys or 'query' in key:
                info.pop(key, None)

        response_string += str(movie)
        return response_string

    def dispatch(self, command_string):
        # TODO - This function takes in as input a string command (e.g. 'insert_movie')
        # the return value of the function should depend on the command
        # For commands 'insert_movie', 'delete_movie', 'update_movie', delete_table' :
        #       return the message as a string that is expected as the output of the command
        # For commands 'search_movie_actor', 'search_movie_actor_director', print_stats' :
        #       return the a list of json objects where each json object has only the required
        #       keys and attributes of the expected result items.

        # Note: You should not print anything to the command line in this function.
        parts = command_string.split(" ")
        response = ''

        if parts[0] == 'insert_movie':
            responses = prompt_user(["Year>", "Title>", "Directors>", "Actors>", "Release Date>",
                                    "Rating>"])
            temp_directors = responses[2]
            temp_actors = responses[3]

            pattern = re.compile("^\s+|\s*,\s*|\s+$")
            directors = [director for director in pattern.split(temp_directors) if director]
            actors = [actor for actor in pattern.split(temp_actors) if actor]

            response = self.insert_movie(responses[0], responses[1], directors, actors, responses[4], responses[5])

        elif parts[0] == 'delete_movie':
            responses = prompt_user(["Title>"])
            response = self.delete_movie_by_title(responses[0])

        elif parts[0] == 'update_movie':
            responses = prompt_user(["Year>", "Title>", "Directors>", "Actors>", "Release Date>",
                                    "Rating>"])
            temp_directors = responses[2]
            temp_actors = responses[3]

            pattern = re.compile("^\s+|\s*,\s*|\s+$")
            directors = [director for director in pattern.split(temp_directors) if director]
            actors = [actor for actor in pattern.split(temp_actors) if actor]

            response = self.update_movie(responses[0], responses[1], directors, actors, responses[4], responses[5])

        elif parts[0] == 'search_movie_actor':
            responses = prompt_user(["Actor name>"])
            response = self.search_movie_actor(responses[0])

        elif parts[0] == 'search_movie_actor_director':
            responses = prompt_user(["Actor name>", "Director name>"])
            response = self.search_movie_actor_director(responses[0], responses[1])

        elif parts[0] == 'print_stats':
            responses = prompt_user(["stats>"])
            if responses[0] == "highest_rating_movie" or responses[0] == "lowest_rating_movie":
                response = self.print_stats(responses[0])
            else:
                print("Unrecognized statistic query " + responses[0])

        elif parts[0] == 'delete_table':
            responses = prompt_user(["table_name>"])
            response = self.delete_table(responses[0])

        elif parts[0] == 'get_movie':
            if sys.version_info[0] < 3:
                title = raw_input("Title>")
            else:
                title = input("Title>")

            response = self.get_movie(title)

        return response


def main():
    # TODO - implement the main function so that the required functionality for the program is achieved.
    dynamo_handler = DynamoDBHandler(region="us-east-1")

    # create the movies table
    dynamo_handler.create_and_load_data(tableName="Movies", fileName="TODO: !!!!!!!!") 

    while True:
        try:
            command_string = ''
            if sys.version_info[0] < 3:
                command_string = raw_input("Enter command ('help' to see all commands, 'exit' to quit)>")
            else:
                command_string = input("Enter command ('help' to see all commands, 'exit' to quit)>")
    
            # Remove multiple whitespaces, if they exist
            command_string = " ".join(command_string.split())
            
            if command_string == 'exit':
                print("Good bye!")
                #dynamo_handler.delete_table()
                exit()
            elif command_string == 'help':
                dynamo_handler.help()

            else:
                response = dynamo_handler.dispatch(command_string)
                print(response)
        except Exception as e:
            print(e)

def prompt_user(titles):
    array_of_responses = []
    for title in titles:
        if sys.version_info[0] < 3:
            response = raw_input(title)
            array_of_responses.append(response)
        else:
            response = input(title)
            array_of_responses.append(response)
    return array_of_responses


class DecimalEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, decimal.Decimal):
            if o % 1 > 0:
                return float(o)
            else:
                return int(o)
        return super(DecimalEncoder, self).default(o)

if __name__ == '__main__':
    main()
